import React from 'react';
import { View, Image, Text, StyleSheet } from 'react-native';

const CocktailTile = (props) => {
  const {
    name,
    ingredients,
    glass: { name: glassName },
    instructions,
    missingIngredients,
    imgUrl,
  } = props.cocktail;

  return (
    <View style={[styles.container, missingIngredients.length ? styles.containerMissingIng: '']}>
      <Image
        source={{ uri: imgUrl }}
        style={{
          width: 250,
          height: 250,
          resizeMode: 'contain',
        }}
      />
      <Text style={styles.name}>
        {name}
      </Text>
      <Text style={styles.instructions}>
        {instructions}
      </Text>
      <View style={styles.glass}>
        <Text style={{ color: '#335fcd', fontWeight: 'bold' }}>Glass: {glassName}</Text>
      </View>
      <View style={styles.ingredients}>
        <Text style={styles.ingredientsTitle}>Ingredients: </Text>
        <View style={styles.ingredientsItems}>
          {
            ingredients.map(item => (
              <View style={styles.listItemContainer} key={item.id}>
                <Text style={styles.listItem}>
                <Text style={{ color: '#3bcd49', fontSize: 14 }}>✔</Text>  {item.name} {item.volume} ({item.alcoholic ? 'alcoholic' : 'non-alcoholic'})</Text>
              </View>
            ))
          }
        </View>
      </View>
      {missingIngredients.length ? (
        <View style={styles.ingredients}>
          <Text style={styles.missingIngredientsTitle}>Missing ingredients: </Text>
          <View style={styles.ingredientsItems}>
            {
              missingIngredients.map(item => (
                <View style={styles.listItemContainer} key={item.id}>
                  <Text style={styles.listItem}>
                    <Text style={{ color: '#3bcd49', fontSize: 14 }}>❌</Text>  {item.name} {item.volume} ({item.alcoholic ? 'alcoholic' : 'non-alcoholic'})</Text>
                </View>
              ))
            }
          </View>
        </View>
      ) : null}
    </View>
  );
}

export default CocktailTile;

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    borderRadius: 4,
    width: '90%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 10,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 10,
    margin: '2%',
    shadowOpacity: 0.75,
    shadowRadius: 5,
    shadowColor: 'rgba(0,0,0,0.3)',
    shadowOffset: { height: 0, width: 0 },
    fontFamily: 'SourceSansPro',
  },
  containerMissingIng: {
    opacity: 0.8,
  },
  name: {
    marginTop: 10,
    fontWeight: 'bold',
    color: '#dd8ae2'
  },
  instructions: {
    marginTop: 10,
    color: '#4c4848',
    fontSize: 14,
    fontStyle: 'italic',
  },
  glass: {
    marginTop: 10,
  },
  ingredients: {
    width: '100%',
    marginTop: 10,
  },
  ingredientsTitle: {
    margin: 'auto',
    color: '#3bcd49',
    fontWeight: 'bold',
    marginBottom: 5,
  },
  ingredientsItems: {
    width: '100%',
    display: 'flex',
  },
  listItemContainer: {
    width: '100%',
    display: 'flex',
  },
  listItem: {
    marginTop: 5,
    fontSize: 12,
    fontStyle: 'italic',
    color: '#4c4848',
  },
  missingIngredientsTitle: {
    margin: 'auto',
    color: '#db7171',
    fontWeight: 'bold',
    marginBottom: 5,
  },
});
