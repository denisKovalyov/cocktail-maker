import React from 'react';
import { View, Image, Text, StyleSheet, TouchableWithoutFeedback } from 'react-native';

const DrinkTile = (props) => {
  return (
    <TouchableWithoutFeedback onPress={props.onPress}>
      <View style={[
        styles.container,
        props.selected ? styles.selected : '',
      ]}>
        {props.selected && (
          <View style={{
            position: 'absolute',
            zIndex: 1,
            top: 5,
            right: 5,
            display: 'flex',
            width: 20,
            height: 20,
            borderRadius: 100,
            background: '#3bcd49',
          }}>
            <Text style={{ color: '#fff', margin: 'auto' }}>✅</Text>
          </View>
        )}
        <Image
          source={{ uri: props.src }}
          style={{
            width: 120,
            height: 120,
            resizeMode: 'contain',
          }}
        />
        <Text style={styles.name}>
          {props.name}
        </Text>
      </View>
    </TouchableWithoutFeedback>
  );
}

export default DrinkTile;

const styles = StyleSheet.create({
  container: {
    position: 'relative',
    backgroundColor: '#fff',
    borderRadius: 4,
    width: '46%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 10,
    paddingLeft: 5,
    paddingRight: 5,
    paddingBottom: 5,
    margin: '2%',
    shadowOpacity: 0.75,
    shadowRadius: 5,
    shadowColor: 'rgba(0,0,0,0.3)',
    shadowOffset: { height: 0, width: 0 },

  },
  name: {
    margin: 10,
    fontWeight: 'bold',
    fontFamily: 'SourceSansPro',
    color: '#4c4848'
  }
});
