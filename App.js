import React, { useEffect, useState } from 'react';
import { Button, ScrollView, StyleSheet, TextInput, View } from 'react-native';
import axios from 'axios';
import { StatusBar } from 'expo-status-bar';
import { useFonts } from 'expo-font';
import DrinkTile from './components/DrinkTile';
import CocktailTile from './components/CocktailTile';
import Loader from './components/Loader';

const getCocktail = async (ingredients) => await new Promise((resolve) => {
  setTimeout(() => {
    resolve(axios.post('https://clm.edroid.org/api/v1/cocktails/search', ingredients))
  }, 1000);
});

export default function App() {
  const [isLoading, setIsLoading] = useState(false);
  const [search, setSearchText] = useState('');
  const [drinks, setDrinks] = useState([]);
  const [selectedDrinks, setSelectedDrinks] = useState([]);
  const [cocktails, setCocktails] = useState([]);

  useEffect(() => {
    const getDrinks = async () => {
      const { data } = await axios.get('https://clm.edroid.org/api/v1/ingredients');
      setDrinks(data);
    };
    getDrinks();
  }, []);

  let [fontsLoaded] = useFonts({
    'SourceSansPro': require('./assets/fonts/SourceSansPro-Regular.otf'),
  });

  if (!fontsLoaded || isLoading) {
    return <Loader/>;
  } else {
    return (
      <View style={styles.container}>
        {!cocktails.length && (
          <TextInput
            style={styles.search}
            onChangeText={setSearchText}
            value={search}
          />
        )}
        <ScrollView
          style={styles.scrollView}
          contentContainerStyle={{ justifyContent: 'space-around', flexWrap: 'wrap', flexDirection: 'row' }}>
          {!cocktails.length ? drinks.filter((item) => item.name.toLowerCase().startsWith(search.toLowerCase())).map((drink) => (
            <DrinkTile
              key={drink.id}
              src={drink.image}
              name={drink.name}
              selected={selectedDrinks.includes(drink.id)}
              onPress={() => {
                if (selectedDrinks.includes(drink.id)) {
                  setSelectedDrinks(selectedDrinks => selectedDrinks.filter(item => item !== drink.id))
                } else {
                  setSelectedDrinks(selectedDrinks => [...selectedDrinks, drink.id])
                }
              }}
            />)
          ) : cocktails.map((cocktail) => (<CocktailTile
              key={cocktail.id}
              cocktail={cocktail}
            />
          ))}
        </ScrollView>
        <View style={styles.buttonContainer}>
          <View style={[styles.innerButtonContainer, cocktails.length ? styles.cocktailsButton : styles.ingredientsButton,
            !selectedDrinks.length && !cocktails.length ? styles.disabledButton : '',
          ]}>
            <Button
              title={cocktails.length ? 'Cheers!' : 'Get your cocktail!'}
              color="#fff"
              backgroundColor={cocktails.length ? '#dd8ae2' : '#3bcd49'}
              disabled={!selectedDrinks.length && !cocktails.length}
              onPress={async () => {
                if (!cocktails.length) {
                  setIsLoading(true);
                  const { data } = await getCocktail(selectedDrinks);
                  setCocktails(data);
                  setSearchText('');
                  setIsLoading(false);
                } else {
                  setCocktails([]);
                  setSelectedDrinks([]);
                }
              }}
            />
          </View>
        </View>
        <StatusBar style="auto" />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 30,
    paddingBottom: 10,
    paddingLeft: 10,
    paddingRight: 10,
    alignItems: 'center',
    backgroundColor: '#d4f5c5',
  },
  buttonContainer: {
    position: 'absolute',
    zIndex: 1,
    display: 'flex',
    alignItems: 'center',
    bottom: 0,
    left: 0,
    right: 0,
    padding: 10,
    height: 60,
    backgroundColor: '#d4f5c5',
  },
  innerButtonContainer: {
    width: '96%',
    borderRadius: 4,
  },
  scrollView: {
    width: '100%',
    flex: 1,
    marginBottom: 50,
  },
  search: {
    margin: 10,
    width: '96%',
    height: 30,
    borderRadius: 4,
    paddingLeft: 6,
    paddingTop: 4,
    paddingBottom: 4,
    backgroundColor: '#fff',
    fontFamily: 'SourceSansPro',
    color: '#4c4848',
  },
  cocktailsButton: {
    width: '90%',
    margin: 'auto',
    backgroundColor: '#e34aec',
  },
  ingredientsButton: {
    backgroundColor: '#3bcd49',
  },
  disabledButton: {
    backgroundColor: '#6e9171',
  },
});
